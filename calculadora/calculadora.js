var numero1;
var numero2;
var operacion;
var cambio;
var resultado;
var punt = false;
var muestro = false;
var emme;
var histor = new Array();

function numero(num){
    document.getElementById('num' + num).style.background = 'gray';
    if (resultado!='')
        limpiar()
    if (cambio=='')
        document.getElementById('numero').value = document.getElementById('numero').value + num.toString()
    else{
        document.getElementById('numero').value = num;
        cambio = ''
    }
    setTimeout(function() {
        document.getElementById('num' + num).style.background = '#ccc';        
    }, 100);
}

function punto(){
    if (!punt){
        document.getElementById('numero').value = document.getElementById('numero').value + '.';
        punt = true;
    }
}

function limpiar(){
    document.getElementById('limpiar').style.background = 'gray';
    document.getElementById('numero').value = '';
    numero1 = 0;
    numero2 = 0;
    cambio = '';
    resultado = '';
    punt = false;
    setTimeout(function() {
        document.getElementById('limpiar').style.background = '#ccc';        
    }, 100);
}

function oper(op){
    document.getElementById(op).style.background = 'gray';
    numero1 = parseFloat(document.getElementById('numero').value);
    operacion = op;
    cambio = 'a';
    punt = false;
    setTimeout(function() {
        document.getElementById(op).style.background = '#ccc';        
    }, 100);
}

function trigo(tip){
    switch (tip){
        case 'sin':
            document.getElementById('numero').value = Math.sin(parseFloat(document.getElementById('numero').value));
            break;
        case 'cos':
            document.getElementById('numero').value = Math.cos(parseFloat(document.getElementById('numero').value));
            break;
        case 'tan':
            document.getElementById('numero').value = Math.tan(parseFloat(document.getElementById('numero').value));
            break;
        default: break;
        }
}

function igual(){
    var sign;
    document.getElementById('igual').style.background = 'gray';
    numero2 = parseFloat(document.getElementById('numero').value);
    switch(operacion)
    {
        case 'mas':{
            resultado = numero1 + numero2;
            sign = '+';
        } break;
        case 'menos':{
            resultado = numero1 - numero2;
            sign = '-';
        } break;
        case 'por':{
            resultado = numero1 * numero2;
            sign = 'x';
        } break;
        case 'entre':{
            resultado = numero1 / numero2;
            sign = '/';
        } break;
        default: break;
    }
    document.getElementById('numero').value = resultado;
    histor.push(numero1.toString() + sign + numero2.toString() + '=' + resultado.toString() + '<br>');
    setTimeout(function() {
        document.getElementById('igual').style.background = '#ccc';        
    }, 100);
}

function historial(){
    document.getElementById('histori').innerHTML = histor;
}

function tecla(event){
    var x = event.keyCode;
    switch (x){
        case 48: numero(0); break;
        case 49: numero(1); break;
        case 50: numero(2); break;
        case 51: numero(3); break;
        case 52: numero(4); break;
        case 53: numero(5); break;
        case 54: numero(6); break;
        case 55: numero(7); break;
        case 56: numero(8); break;
        case 57: numero(9); break;
        default: break;
    }
}

function mmas(){
    emme = parseFloat(document.getElementById('numero').value);
}

function mmenos(){
    emme = 0;
}

function eme(){
    document.getElementById('numero').value = emme;
}

function mostrare(iddiv){
    if (muestro)
        borrarCalc(iddiv);
    else
        crearCalc(iddiv);
    muestro = !muestro;
}

function crearCalc(iddiv){
    agregarBR(iddiv);
    agregarElemento("sin", "sin", "trigo('sin')", iddiv);
    agregarElemento("cos", "cos", "trigo('cos')", iddiv);
    agregarElemento("tan", "tan", "trigo('tan')", iddiv);
    agregarElemento("Hist", "historial", "historial()", iddiv);
    agregarBR(iddiv);

    agregarElemento("M+", "mmas", "mmas()", iddiv);
    agregarElemento("M-", "mmenos", "mmenos()", iddiv);
    agregarElemento("M", "eme", "eme()", iddiv);
    agregarElemento("/", "entre", "oper('entre')", iddiv);
    agregarBR(iddiv);

    agregarElemento("7", "num7", "numero('7')", iddiv);
    agregarElemento("8", "num8", "numero('8')", iddiv);
    agregarElemento("9", "num9", "numero('9')", iddiv);
    agregarElemento("+", "mas", "oper('mas')", iddiv);
    agregarBR(iddiv);

    agregarElemento("4", "num4", "numero('4')", iddiv);
    agregarElemento("5", "num5", "numero('5')", iddiv);
    agregarElemento("6", "num6", "numero('6')", iddiv);
    agregarElemento("-", "menos", "oper('menos')", iddiv);
    agregarBR(iddiv);

    agregarElemento("1", "num1", "numero('1')", iddiv);
    agregarElemento("2", "num2", "numero('2')", iddiv);
    agregarElemento("3", "num3", "numero('3')", iddiv);
    agregarElemento("x", "por", "oper('por')", iddiv);
    agregarBR(iddiv);

    agregarElemento("CE", "limpiar", "limpiar()", iddiv);
    agregarElemento("0", "num0", "numero('0')", iddiv);
    agregarElemento(".", "punto", "punto()", iddiv);
    agregarElemento("=", "igual", "igual()", iddiv);
}

function borrarCalc(iddiv){
    borrarElemento("sin", iddiv);
    borrarElemento("cos", iddiv);
    borrarElemento("tan", iddiv);
    borrarElemento("historial", iddiv);

    borrarElemento("mmas", iddiv);
    borrarElemento("mmenos", iddiv);
    borrarElemento("eme", iddiv);
    borrarElemento("entre", iddiv);

    borrarElemento("num7", iddiv);
    borrarElemento("num8", iddiv);
    borrarElemento("num9", iddiv);
    borrarElemento("mas", iddiv);

    borrarElemento("num4", iddiv);
    borrarElemento("num5", iddiv);
    borrarElemento("num6", iddiv);
    borrarElemento("menos", iddiv);

    borrarElemento("num1", iddiv);
    borrarElemento("num2", iddiv);
    borrarElemento("num3", iddiv);
    borrarElemento("por", iddiv);

    borrarElemento("limpiar", iddiv);
    borrarElemento("num0", iddiv);
    borrarElemento("punto", iddiv);
    borrarElemento("igual", iddiv);
    var cell = document.getElementsByTagName('br');
    var length = cell.length;
    for(var i = 0; i < length; i++) {
        cell[0].parentNode.removeChild(cell[0]);
    }
}

function agregarElemento(texto, aidi, funcion, iddiv){
    var para = document.createElement("button");
    var node = document.createTextNode(texto);
    para.appendChild(node);
    para.setAttribute("id", aidi);
    para.setAttribute("onclick", funcion);
    var element = document.getElementById(iddiv);
    element.appendChild(para);
}

function agregarBR(iddiv){
    var para = document.createElement("br");
    var element = document.getElementById(iddiv);
    element.appendChild(para);
}

function borrarElemento(aidi, iddiv){
    var parent = document.getElementById(iddiv);
    var child = document.getElementById(aidi);
    parent.removeChild(child);
}