var start, end;
var equis = [];
var ye = [];

function graficar(){
    var canvas = document.getElementById('grafica');
    var context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width, canvas.height);
    plano();

    context.beginPath();
    context.moveTo(250 + equis[0]*25, 250 - ye[0]*25);
    for(i = 1; i <= equis.length - 1; i++){
        context.lineTo(250 + equis[i]*25, 250 - ye[i]*25);
        context.stroke();
    }
    
}

function plano(){
    var canvas = document.getElementById('grafica');
    var context = canvas.getContext('2d');
    context.beginPath();
    context.moveTo(250,0);
	context.lineTo(250,500);
   	context.stroke();
    context.beginPath();
    context.moveTo(0,250);
	context.lineTo(500,250);
   	context.stroke();

    context.beginPath();
    for (var i = 0; i < 26; i++) {
        context.moveTo(25*i, 245);
        context.lineTo(25*i, 255);
        context.stroke();
    }

    context.beginPath();
    for (var i = 0; i < 26; i++) {
        context.moveTo(245, 25*i);
        context.lineTo(255, 25*i);
        context.stroke();
    }
}

function ecuacion(){
    equis = [];
    ye = [];
    var a = parseFloat(document.getElementById('ax2').value);
    var b = parseFloat(document.getElementById('bx').value);
    var c = parseFloat(document.getElementById('cc').value);
    var start = document.getElementById('iniciox').value;
    var end = document.getElementById('finx').value;

    for (i = start; i <= end; i++){
        var x,y;
        x = i;
        y = (Math.pow((a*x),2) + (b*x) + c);
        equis.push(x);
        ye.push(y);
    }
    graficar();
}